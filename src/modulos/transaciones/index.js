const Historial = require('../../models/Historial'); 


const handlers = () => ({

    transferir: async(req, res) => {
        try{
        const { monto } = req.body;

        if( monto <= 0 || !monto) {

            res.json({
                msg: 'elmonto debe ser mayor a 0'
            });
        }

        const operacion = new Historial(req.body);

            await operacion.save();
        
        res.json(operacion);    

        }catch (error) {
            console.log(error);
        }    
    },

    historial: async(req, res) => {
        try{
            const cartola = await Historial.find();

            if(!cartola) {
                res.json({
                    status: 400,
                    msg: 'ha sucedido un error'
                });
            }
        

            res.json({cartola});

        }catch(error) {
            console.log(error);
        }

    }
});    

module.exports = handlers;