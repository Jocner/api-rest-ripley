const mongoose = require('mongoose');

const ClientesSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    rut: {
        type: String,
        required: true,
        trim: true, 
        unique: true
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    telefono: {
        type: String,
        require: true,
        trim: true
    },
    bancodestino: {
        type: String,
        require: true,
        trim: true
    },
    tipocuenta: {
        type: String,
        require: true,
        trim: true
    },
    numerocuenta: {
        type: String,
        require: true,
        trim: true
    },
    registro: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('Cliente', ClientesSchema);